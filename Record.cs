﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleLibrary
{
    class Record
    {
        public Book book;
        public int Day;
        public Record(Book book, int day)
        {
            this.book = book;
            Day = day;
        }
        public string ShowJournal()
        {
            return book.GetTitle() + " | was taken for " + Day + " days";

        }
    }
}
