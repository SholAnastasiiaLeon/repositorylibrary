﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleLibrary
{
    class Library
    {
          
          
   
        Menu menu = new Menu();
        public List<Record> journal;    // журнал для записи взятых книг
        public List<Book> books;// список книг
        public Library()    //конструктор
        {
            books = new List<Book>();
            journal = new List<Record>();
            books.Add(new Book(" ", " "));
            books.Add(new Book("Western Front", "Erich Remark"));
            books.Add(new Book("Road", "Cormac McCarthy"));
            books.Add(new Book("The Shining", "Stephen King"));
            books.Add(new Book("Humankind", "Yuval Harari"));
            books.Add(new Book("The Stand", "Stephen King"));
        }

        public void AddBook()
        {
            Console.WriteLine();
            menu.AddTitleMenu();
            string title = Convert.ToString(Console.ReadLine());
            menu.Done();
            menu.AddAuthorMenu();
            string author = Convert.ToString(Console.ReadLine());
            menu.Done();
            Book book = new Book(title, author);
            books.Add(book);

        }
        public void RemoveBook()
        {
            Console.WriteLine("Enter number of book which you want to remove");
            int bookNumber = Convert.ToInt32(Console.ReadLine());
            books.RemoveAt(bookNumber);            //удаление элемента из списка по указанному индексу index
            menu.Done();

        }

        public int СomparerTitle(Book x, Book y)
        {
            return x.GetTitle().CompareTo(y.GetTitle());
        }

        public int СomparerAuthor(Book x, Book y)
        {
            return x.GetAuthor().CompareTo(y.GetAuthor());
        }
        public void SortLibrary(bool i)// i - true, else - false
        {
            if (i) books.Sort(СomparerTitle);   //true
            else books.Sort(СomparerAuthor);    //false  
        }
        // выводим список книг
        public void Print()
        {
            for (int i = 1; i < books.Count; i++)
            {
                Console.WriteLine(i + "- " + books[i].ShowBooksList());
            }
        }

        public void SearchAuthor()
        {
            string author = Convert.ToString(Console.ReadLine());
            int numberOfBooks = 0;  //количество книг в списке
            for (int i = 0; i < books.Count; i++)
            {
                if (books[i].GetAuthor() == author)
                {
                    Console.WriteLine(books[i].GetTitle());
                    numberOfBooks++;
                }
            }
            if (numberOfBooks == 0)
            {
                Console.WriteLine("There aren't such books");
            }
        }
        public void TakeBook()
        {
            int numberOfTakenBook = Convert.ToInt32(Console.ReadLine());  // номер книги которая берется
            int dayBookTaken = Convert.ToInt32(Console.ReadLine());  // количество дней на которые берется книга
            journal.Add(new Record(books[numberOfTakenBook], dayBookTaken));       // добавление выбранной книги в журнал
            books[numberOfTakenBook].AddDays(dayBookTaken);
        }
        public void PrintJournal()
        {
            if (journal.Count == 0)
            {
                Console.WriteLine("You haven't taken the books yet");
            }
            else if (journal.Count > 0)
            {
                foreach (Record record in journal)
                {
                    Console.WriteLine(record.ShowJournal());
                }
            }

        }
    } 
}
