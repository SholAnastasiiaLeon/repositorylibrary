﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleLibrary
{
    class Book
    {
        public string Author;
        public string Title;
        public int AmountDaysRent;

        public string ShowBooksList()
        {
            return Author + " " + Title + " the book was taken for " + AmountDaysRent + " days";
        }
        public Book(string title, string author)
        {
            Author = author;
            Title = title;
            AmountDaysRent = 0;
        }
        public void AddDays(int day)
        {
            AmountDaysRent += day;
        }

        public string GetTitle()
        {
            return Title;
        }

        public string GetAuthor()
        {
            return Author;
        }
    }
}
