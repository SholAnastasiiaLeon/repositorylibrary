﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleLibrary
{
    class Menu
    {
        public void ShowMenu()
        {
            Console.WriteLine("1. Add new book to the library");
            Console.WriteLine("2. Remove current book from library");
            Console.WriteLine("3. Sort by names of the title");
            Console.WriteLine("4. Sort by names of the author");
            Console.WriteLine("5. Print list of books");
            Console.WriteLine("6. List of taken books");
            Console.WriteLine("7. Add book to taken books");
            Console.WriteLine("8. Search author");
        }
        public void MainMenu()
        {
            Console.WriteLine("Make your choice (Press 9 for show menu)");
        }
        public void AddTitleMenu()
        {
            Console.WriteLine("Add title of book");
        }
        public void AddAuthorMenu()
        {
            Console.WriteLine("Add author of book");
        }
        public void Done()
        {
            Console.WriteLine("Done");
        }
        public void TakeBookMenu()
        {
            Console.WriteLine("Enter book numbers");
            Console.WriteLine("Enter amount of days to take a book");
        }
    }
}
