﻿using LittleLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleLibrary
{
    class Program
    {

        static void Main(string[] args)
        {
            Menu menu = new Menu();
            Library library = new Library();
            char input;
            menu.ShowMenu();
            do
            {
                Console.WriteLine("Make your choice (Press 9 for show menu)");
                input = Convert.ToChar(Console.ReadLine());
                switch (input)
                {
                    case '1':       //добавить метод добавления книги
                        library.AddBook();
                        break;
                    case '2':       //метод удаления книги
                        library.RemoveBook();
                        break;
                    case '3':       //добавить метод сортировки по автору
                        library.SortLibrary(true);
                        library.Print();
                        break;
                    case '4':       //добавить метод сортировки по книге
                        library.SortLibrary(false);
                        library.Print();
                        break;      //метод для показа списока книг
                    case '5':
                        library.Print();
                        break;
                    case '6':       //журнал взятых книг
                        library.PrintJournal();
                        break;
                    case '7':       //метод для записи книги в журнал книг
                        menu.TakeBookMenu();
                        library.TakeBook();
                        menu.Done();
                        break;
                    case '8':       //метод поиска по автору
                        library.SearchAuthor();
                        break;
                    case '9':       //повторный вывод главного меню
                        menu.ShowMenu();
                        break;
                    default:
                        Console.WriteLine("Incorrect input");
                        break;
                }
            } while (true);
            //conflict string is here
        }
    }
}
